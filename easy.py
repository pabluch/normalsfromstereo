import numpy as np
import cv2, math,logging, sys
import os.path

from scipy.misc import imread
from scipy import sparse
from scipy import optimize

import matplotlib
#matplotlib.use('Agg')
from matplotlib import pyplot as plt




mask_path   = None
debug		= logging.INFO

logger	    = logging.getLogger('mniam')
lhandler    = logging.StreamHandler()
lformatter  = logging.Formatter(fmt='%(levelname)s: %(message)s')
lhandler.setFormatter(lformatter)
logger.addHandler(lhandler)


def getImage(filename):
    """Open image file in greyscale mode (intensity)."""
    return imread(filename, flatten=True)

def get_light_poss(in_dir):
	def inside_sphere(x,y,xr,yr,rr):
		return (xr-x)**2+(yr-y)**2 <= rr*rr

	default_masks = ["probe_mask.png","probe_mask.jpg","probe_mask.joeg"]
	
	light_poss = {}
	for default_mask in default_masks:
		if os.path.isfile(in_dir +"//" +default_mask):
			mask_path = in_dir +"//" +default_mask
			break 

	mask = None
	circles_m = None
	if mask_path:
		mask	= cv2.imread(mask_path,0)
	else:
		logger.info("No mask found (probe_mask.png|jpg|jpeg) in %s - skipping light detection."%in_dir)
		return None

	for dir, subd, files in os.walk(in_dir):
		for file in files:
			_file = os.path.join(dir, file)
			logger.debug(_file)
			if default_mask in file:
				continue
			
			img	= cv2.imread(_file,0)
			res	= cv2.bilateralFilter(img,9,79,79)

			if mask is not None:
				res	= cv2.bitwise_and(img, img, mask = mask )
				circles_m = cv2.HoughCircles(mask, cv2.HOUGH_GRADIENT, 1, 25, param1=50,param2=30,minRadius=10,maxRadius=0)
				circles_m = np.round(circles_m[0,:]).astype("int").tolist()		
			
			test_output = cv2.cvtColor(res,cv2.COLOR_GRAY2BGR)
			circles = cv2.HoughCircles(res, cv2.HOUGH_GRADIENT, 1, 40, param1=50,param2=30,minRadius=10,maxRadius=0)

			sphere_x, sphere_y, sphere_r = 0,0,0
			light_x, light_y, light_r = 0,0,0

			if circles is None or len(circles) == 0:
				logger.warning("no test sphere detected - discarding (%s)"%_file )
				continue
			
			circles = np.round(circles[0,:]).astype("int").tolist()			
			circles = circles + circles_m
			circles = sorted(circles,  key=lambda x: x[2], reverse=True) #sort on r

			if float(circles[0][2])/float(circles[-1][2]) < 2:
				sphere_x, sphere_y, sphere_r = circles[0]
				(minVal, maxVal, minLoc, maxLoc) = cv2.minMaxLoc(res)
				light_x, light_y = maxLoc
				if not inside_sphere( light_x, light_y, sphere_x, sphere_y, sphere_r):
					logger.warning("light reflection outside mask sphere - discarding (%s)"%_file)
					continue  
			else:
				sphere_x, sphere_y, sphere_r = circles[0]
				light_x, light_y, light_r = circles[-1]
				if not inside_sphere( light_x, light_y, sphere_x, sphere_y, sphere_r):
					logger.warning("light reflection outside the sphere - discarding (%s)"%_file)
					continue
	
			for (x, y, r) in [(sphere_x, sphere_y, sphere_r), (light_x, light_y, light_r)]:
				cv2.circle(test_output, (x, y), r, (0, 255, 0), 1)
				cv2.rectangle(test_output, (x - 5, y - 5), (x + 5, y + 5), (0, 128, 255), -1)
			
			lightv = [0,0,0]
			lightv[0] = light_x - sphere_x
			lightv[1] = light_y - sphere_y
			lightv[2] = math.sqrt( sphere_r**2 - lightv[0]**2 - lightv[1]**2)
			lightv				/= np.linalg.norm(lightv)
			light_poss[_file]	= (lightv[2],lightv[1],lightv[0])

			if debug == logging.DEBUG: 
				cv2.imshow('image',test_output)	
				key = cv2.waitKey(0)

	if debug == logging.DEBUG: 
		cv2.destroyAllWindows()

	if len(light_poss) == 0:
		light_poss = None

	return light_poss


def photometricStereo(lightning):
    """Based on Woodham '79 article.
    I = Matrix of input images, rows being different images.
    N = lightning vectors
    N_i = inverse of N
    rho = albedo of each pixels
    """

    images = list( map(getImage, lightning.keys() ))
    logger.debug( images )

    n = len(lightning)
    I = np.vstack(x.ravel() for x in images)
    output = np.zeros((3, I.shape[1]))
    N = np.vstack(x for x in lightning.itervalues())
    N_i = np.linalg.pinv(N)
    rho = np.linalg.norm(N_i.dot( I ), axis=0)
    I = I / rho
    normals, residual, rank, s = np.linalg.lstsq(N, I[:, rho != 0].reshape(n, -1))
    output[:,rho != 0] = normals
    w, h = images[0].shape
    output = output.reshape(3, w, h).swapaxes(0, 2)
    # TODO: Raise an error on misbehavior of lstsq.

    return output


def colorizeNormals(normals):
    """Generate an image representing the normals."""
    # Normalize the normals
    nf = np.linalg.norm(normals, axis=normals.ndim - 1)
    normals_n = normals / np.dstack((nf, nf, nf))

    color = (normals_n + 1) / 2

    return color

def main( argv ):
	global debug
	
	if "--debug" in argv:
		debug = logging.DEBUG

	logger.setLevel(debug)
	
	args = argv
	logger.debug(args)
	if len(args) == 0:
		 args = ['tests2']
	elif not os.path.isdir( args[0]):
		print "need dir with files as an arg..."
		return 1
	
	light_poss	= get_light_poss( args[0] )
	logger.debug( light_poss )

	normals	    = photometricStereo(light_poss)
	color		= colorizeNormals(normals)
	
	plt.imsave('out.png', color)
	#cv2.imsave('out.png', color)


if __name__ == "__main__":
    main(sys.argv[1:])